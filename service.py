# -*- coding: utf-8 -*-

import requests
import json

def handler(incoming, context):
    event = incoming['body']
    if isinstance(incoming['body'], str):
        event = json.loads(incoming['body'])

    print(event)

    # Poor's man async request
    try:
        requests.post(
            'https://zhktfieuhf.execute-api.eu-west-3.amazonaws.com/budziol/budziol',
            data=json.dumps(event),
            timeout=2,
            headers={
                "Content-Type": "application/json;charset=UTF-8"
            }
        )
    except requests.exceptions.ReadTimeout:
        print("All's good")
    except:
        print("Could not connect")
        raise

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "access-control-allow-origin": "*",
            "access-control-allow-methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
            "access-control-allow-headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token"
        }
    }
